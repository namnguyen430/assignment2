from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)


books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]


import json
@app.route('/book/JSON')
def bookJSON():
    return json.dumps(books, indent=4)

@app.route('/')
def index():
    return render_template('showBook.html', books = books)
@app.route('/book/', methods=['GET', 'POST'])
def showBook():
    return render_template('showBook.html', books = books)
    
    
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        books.append({'title': request.form['name'], 'id': str(len(books)+1)})
        return redirect(url_for('showBook', books = books))
    return render_template('newBook.html', books = books)
    

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        books[book_id-1]['title'] = request.form['name']
        return redirect(url_for('showBook'))
    return render_template('editBook.html', books = books)

    
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        books.remove(books[book_id-1])
        for i in range(len(books)):
            books[i]['id'] = i+1
        return redirect(url_for('showBook', books = books))
    return render_template('deleteBook.html', book = books[book_id-1])
    
    
if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
